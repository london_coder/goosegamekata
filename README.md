# The Goose Game
This is a solution posed as a code test for candidates for the role of Scala Programmer, required by Databiz. The problem statement can be seen in the INSTRUCTIONS.md file.

This document outlines how to copy and run the solution. Including how to get the source, how to run the code locally.

## Background
This is a Scala solution, built with the aid of sbt. To run the solution, you will of course need a version of the Java runtime, e.g. JVM 1.8. Although there are more recent version of the JVM, this exercise was completed with version 1.8. You will need a local instance of sbt. Sbt will be able to provide any dependencies that are not locally available if the user has an internet connection.


## Installation
If you do not have sbt installed on your computer, use your operating system package manager to install it. Instructions can be found [here:](https://www.scala-sbt.org/1.x/docs/Setup.html)

This solution was built using Scala version 2.12.8, and sbt version 1.2.8. You will not need to install Scala if you do not have it, sbt will download the correct version.

- Clone the project, by copying the repository URL from Bitbucket: https://bitbucket.org/london_coder/goosegamekata/src/master/
On the local computer, in a terminal window type: 

```git clone https://london_coder@bitbucket.org/london_coder/goosegamekata.git```

Once the project has been cloned, change directory to the repository location. This should be called goosegamekata.

In a terminal window, at the prompt type sbt (then enter). This may take a while if sbt needs to download dependencies.

### Compilation
At the sbt prompt, type compile (then enter) to compile the source files. 

### Running the code
At the sbt prompt, type run (then enter) to run the application. Interact in the terminal window as suggested in the INSTRUCTIONS.md file that can be viewed either locally, or from the Bitbucket repository location.

Although it was not specified in the requirements, the solution uses ANSI codes to colour and emphasise the interaction. For best results, use a terminal with a black background.

To exit sbt, type exit (then enter) at the sbt prompt.
