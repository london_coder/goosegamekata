import sbt._

object Dependencies {
	// Versions of dependencies
	val Cats = "1.6.0"
	val Sbt  = "1.2.8"
	
	lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
	// lazy val sbt       = "org.scala-sbt" % "sbt"        % Sbt
	// lazy val cats      = "org.typelevel" %% "cats-core" % Cats
	lazy val fansi     = "com.lihaoyi"   %% "fansi"     % "0.2.5"
	// lazy val sbtCmd    = "org.scala-sbt" %% "command"   % Sbt
}
