package com.alanlewis.goosegame

import Show.{emphasis => em, err, warn, info, text}

import sbt._, Keys._
import complete.DefaultParsers._
import internal.util.complete.Parser


sealed trait Command {
	def run: Unit
}

object Exit extends Command {
	override def run: Unit = {
		text("Bye!!")
	}
}

object Help extends Command {
	override def run: Unit = {
		// TODO not part of the instructions
	}
}
// invalid player input
case class Unknown(in: String) extends Command {
	override def run: Unit = {
		val userInput = if(in.length > 0) in else " "

		err(s"Entry ${em(userInput)} is not understood")
	}
}

// behaviour for a player move
case class Move(name: String, roll_1: Option[Int] = None, roll_2: Option[Int] = None) extends Command {
	override def run: Unit = {
		if(Players.contains(name)) {
			val op_roll1 = validRoll(roll_1, "dice1")
			val op_roll2 = validRoll(roll_2, "dice2")
			if(!op_roll1.isEmpty && !op_roll2.isEmpty) 
				text(Game.gameEvent(name, op_roll1.get, op_roll2.get))
		} else {
			err(s"Player ${em(name)} NOT found")
		}
	}
	

	private def validRoll(roll: Option[Int], die: String): Option[Int] = {
		val dieNum = roll.getOrElse(Game.dieRoll)
		if(dieNum < 1 || dieNum > 6) { // incorrect player input
			err(s"${em(die)} value NOT valid: ${em(dieNum)}. Must be between ${em(1)} and ${em(6)}")
			None
		} else {
			Some(dieNum)
		}
	}
}

case class AddPlayer(name: String) extends Command {
	override def run: Unit = {
		if(Players.contains(name)) {
			warn(s"${em(name)}: already existing player")
		} else {
			Players.addPlayer(name)
			info(s"players: ${Players.all.map(em).mkString(", ")}")
		}
	}
}



// Parse the players input
object CommandParser {
   def parser: Parser[Command] = {
      val help = token("help" ^^^ Help)
      val exit = token("exit" ^^^ Exit)

      val addPlayer = {
         val command = token("add player") ~> Space
         val name = token(StringBasic, s"[${em("Name")} of the player]")
         val combinedParser = command ~> name
         combinedParser.map {
            case name => AddPlayer(name)
         }
      }

      val move = {
         val command = token("move") ~> Space
         val playerName = token(StringBasic, s"[${em("Name")} of the player]") <~ Space.?
         val roll1 = token(IntBasic, s"[${em("First roll")}: number between ${em(1)} and ${em(6)}]") <~ Space
         val roll2 = token(IntBasic, s"[${em("Second roll")}: number between ${em(1)} and ${em(6)}]")
         val combinedParser = command ~> playerName ~ (roll1.? ~ roll2.?)
         combinedParser.map {
            case (playerName, (roll1, roll2)) => Move(playerName, roll1, roll2)
         }
      }

      val unknown = {
         val command = token(any)
         val combinedParser = command.*
         combinedParser.map {
            case command => Unknown(command.mkString)
         }
      }

      return help | exit | addPlayer | move | unknown
   }

}