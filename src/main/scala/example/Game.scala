package com.alanlewis.goosegame

import Show.{emphasis => em}
import scala.util.Random

object Game {
	val BEGIN = 0
	val END = 63
	// land on a GOOSE square and move again the same number of spaces
	val THE_GEESE = Vector(5, 9, 14, 18, 23, 27)
	// land on the BRIDGE, and advance 6 spaces
	val THE_BRIDGE = 6

	val START_GAME = em("Start")
	val WIN = em("Wins!!")

	val BRIDGE_LABEL = s"${em("The")} ${em("Bridge")}"

	val GOOSE_LABEL  = s"${em("The")} ${em("Goose")}"

	val END_LABEL = em(END)

	def dieRoll: Int = Random.nextInt(6) + 1

	def label(playerPos: Int): String = { 
		playerPos match {
		   case BEGIN => START_GAME
		   case THE_BRIDGE => BRIDGE_LABEL
		   case x if(THE_GEESE.contains(playerPos)) => s"${em(playerPos)}, $GOOSE_LABEL"
		   case _  => em(playerPos)
	    }
	}

    def gameEvent(player: String, roll1: Int, roll2: Int): String = {
    	val playerCurrPos = Players.playerPos(player)
    	val dispPlayer = em(player)
    	var playerNextPos = playerCurrPos + roll1 + roll2	 // var becuase it may change by where player lands

    	var output = s"$dispPlayer rolls ${roll1}, ${roll2}, $dispPlayer moves from ${label(playerCurrPos)} to "
    	var continue = true

    	while(continue) {
			playerNextPos match {
				case THE_BRIDGE => {
					output += s"$BRIDGE_LABEL. $dispPlayer jumps to "
					playerNextPos += THE_BRIDGE
				}
				case x if(THE_GEESE.contains(playerNextPos)) => {
					output += s"${label(playerNextPos)}. $dispPlayer moves again and goes to "
					playerNextPos += roll1 + roll2
				}
				case y if(playerNextPos == END) => {
					output += s"$END_LABEL. $dispPlayer $WIN"
					continue = false
				}
				case z if(playerNextPos > END) => {
					output += s"$END_LABEL, $dispPlayer bounces! $dispPlayer returns to "
					playerNextPos = END - (playerNextPos - END)
				}
				case _ => {
					output += label(playerNextPos)
					continue = false
				}  
			}
    	}

    	Players.move(player, playerNextPos)

    	for( otherPlayer <- Players.all if(!otherPlayer.equals(player) && Players.playerPos(otherPlayer) == playerNextPos)) {
    		output += s". On ${label(playerNextPos)} there is ${em(otherPlayer)}, who returns to ${label(playerCurrPos)}"
    		Players.move(otherPlayer, playerCurrPos)
    		output
    	}

    	output
    }
}