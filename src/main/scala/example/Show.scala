package com.alanlewis.goosegame

import fansi._

object Show {

	final val PROMPT: String = (Color.LightBlue ++ Bold.On)("==>").toString
	def fprompt(msg: Any): String = (Color.LightBlue ++ Bold.On)(s"$msg").toString

	// colour coded log style messages
	def ferr(msg: Any) : String = Color.Red(s"$msg").toString
	def fwarn(msg: Any): String = Color.Yellow(s"$msg").toString
	def finfo(msg: Any): String = Color.White(s"$msg").toString

	def err(msg: Any) : Unit = println(s"${ferr(msg)} \n")
	def warn(msg: Any): Unit = println(s"${fwarn(msg)} \n")
	def info(msg: Any): Unit = println(s"${finfo(msg)} \n")

	def emphasis(msg: Any): String = Bold.On(s"$msg").toString

	def ftext(msg: Any): String = Color.White(s"$msg").toString
	def fheading(msg: Any): String = Color.Green(s"$msg").toString

	def text(msg: Any): Unit = println(s"${ftext(msg)} \n")
	def heading(msg: Any): Unit = println(s"${fheading(msg)} \n")

	def prompt(msg: Any): String = (Color.Magenta ++ Bold.On)(s"$msg").toString
} 