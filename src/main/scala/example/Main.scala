package com.alanlewis.goosegame

import Show.{fprompt, err, PROMPT, heading, emphasis => em}
import sbt._
import complete.Parser
import scala.util.Try

// Goose Game application 
object Main extends App {
	// game prompt
	val prompt = s"${fprompt("Goose-Game")} $PROMPT"

	val parser = CommandParser.parser


	def gameLoop: Unit = {
		Try(readLine(parser) match {

			case Some(Exit) => Exit.run
			case Some(cmd)  => {
				cmd.run
				gameLoop
			}
			case None       => gameLoop

		}).recover {
			case _ => err("Sorry, I did not understand ")
				gameLoop
		}
	}

	heading(s"Welcome to the ${em("The Goose Game")}")
	gameLoop


	private def readLine[T](parser: Parser[T]): Option[T] = {
		val reader = new sbt.FullReader(
			Some(new File("goose-game.history")),
			parser
		)
		reader
		.readLine(prompt = prompt)
		.flatMap {
			line => Parser.parse(line.replace(",", ""), parser).fold(_ => None, Some(_))
		}
	}
}