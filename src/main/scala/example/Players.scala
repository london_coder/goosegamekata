package com.alanlewis.goosegame


import scala.collection.mutable.Map
import Show._

object Players {
	// table keeping player's name and position
	private val Db = Map.empty[String, Int]

	def playerCount: Int = Db.size

	def contains(name: String): Boolean = Db.contains(name)

	def addPlayer(name: String): Unit = Db += (name -> 0)

	def move(name: String, pos: Int): Unit = Db(name) = pos

	def all: Iterable[String] = Db.keys

	def playerPos(name: String): Int = Db(name) 

}