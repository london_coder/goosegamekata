package com.alanlewis.goosegame

import org.scalatest._
import scala.util.Random._

class GooseGameSpec extends FlatSpec with Matchers {


  "At game initial state, the players table" should "be empty" in {
  	Players.playerCount shouldEqual 0
  }
  /*
   * Adding player(s)
   */
  "Adding a named player" should "add to player database" in {
  	val players = Players.playerCount
  	Players.addPlayer("Jim")
  	Players.playerCount shouldEqual players + 1
  }

  "Trying to add a name that is already playing" should "not be allowed" in {
  	Players.addPlayer("Barry")
  	val players = Players.playerCount
  	Players.addPlayer("Barry")
  	Players.playerCount shouldEqual players
  }

  /*
   * rolling a die
   */
   "Rolling a die" should "result in a number between 1 and 6" in {
   	// try at least 3 times...
   	val rolls = for ( i <- 1 to 8; x = Game.dieRoll ) yield x

   	rolls.foreach ( _ shouldEqual 1 +- 6 )
   }
}
