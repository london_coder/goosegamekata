import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.alanlewis"
ThisBuild / organizationName := "goosegame"

lazy val root = (project in file("."))
  .settings(
    name := "Scala Goose Game",
    libraryDependencies ++= Seq(
    	scalaTest % Test,
	    fansi,
	    "org.scala-sbt" % "sbt" % "1.2.8"
	)
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
